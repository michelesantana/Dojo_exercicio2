class CadastrarPage < SitePrism::Page

  element :button_create, :xpath,'//*[@id="quickcreatetop"]/a'
  element :button_creat_account, :xpath, '//*[@id="quickcreatetop"]/ul/li[1]/a'
  element :name, "#name"
  element :phone_office, "#phone_office"
  element :website, "#website"
  element :email_address, "#Accounts0emailAddress0"
  element :street_home, "#billing_address_street"
  element :city_home, "#billing_address_city"
  element :state_home, "#billing_address_state"
  element :postal_code, "#billing_address_postalcode"
  element :country, "#billing_address_country"
  element :description, "#description"
  element :shipping_address, "#shipping_address_street"
  element :shipping_address_city, "#shipping_address_city"
  element :state_shipping, "#shipping_address_state"
  element :postal_code_shipping, "#shipping_address_postalcode"
  element :country_shipping, "#shipping_address_country"
  element :type, "#account_type"
  element :industry, "#industry"
  element :annual_Revenue, "#annual_revenue"
  element :employees, "#employees"
  element :button_salvar, :css, "#EditView > div.buttons > #SAVE"
end
