class EditarPage < SitePrism::Page

  element :button_all, :xpath, '//*[@id="grouptab_5"]'
  element :button_account, :xpath, '//*[@id="moduleTab_6_Accounts"]'
  element :button_edit, '#edit_button'
  element :fax_phone, '#phone_fax'
  element :save_continue, :css, "#EditView > div.buttons > #SAVE"
end
