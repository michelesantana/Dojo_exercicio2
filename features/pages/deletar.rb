class ExcluirPage < SitePrism::Page

  element :txt_img, :xpath, '//*[@id="MassUpdate"]/div[3]/table/tbody/tr[1]/td[3]/b/a'
  element :button_actions, :xpath, '//*[@id="tab-actions"]/a'
  element :button_delete, '#delete_button'
end
