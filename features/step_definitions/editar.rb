Dado(/^que eu esteja logado no site da empresa$/) do
  LoginPage.new.load
  LoginPage.new.log_in("will", "will")
end

Quando(/^eu acessar a opção contas será exibido as contas cadastradas$/) do
  EditarPage.new.button_all.click
  EditarPage.new.button_account.click
end

Então(/^conseguirei editar a conta$/) do
  ExcluirPage.new.txt_img.click
  ExcluirPage.new.button_actions.click
  EditarPage.new.button_edit.click
  EditarPage.new.fax_phone.set('1120564708')
  EditarPage.new.save_continue.click
end
