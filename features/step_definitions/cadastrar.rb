Quando(/^eu acessar a opção create$/) do
  CadastrarPage.new.button_create.click
end

Quando(/^inserir os dados para novo cadastro com o "([^"]*)"$/) do |nome|
  CadastrarPage.new.button_creat_account.click
  CadastrarPage.new.name.set(nome + rand(1000).to_s)
  CadastrarPage.new.phone_office.set('11966630562')
  CadastrarPage.new.website.set('https://www.facebook.com/michele.silva.315')
  CadastrarPage.new.email_address.set('michelesantana6@icloud.com')
  CadastrarPage.new.street_home.set('Antonio Gandine')
  CadastrarPage.new.city_home.set('São Paulo')
  CadastrarPage.new.state_home.set('São Paulo')
  CadastrarPage.new.postal_code.set('08215-460')
  CadastrarPage.new.country.set('Brasil')
  CadastrarPage.new.description.set('Moro em casa próximo a estação dom bosco')
  CadastrarPage.new.shipping_address.set('Antonio Gandine')
  CadastrarPage.new.shipping_address_city.set('São Paulo')
  CadastrarPage.new.state_shipping.set('São Paulo')
  CadastrarPage.new.postal_code_shipping.set('08215-460')
  CadastrarPage.new.country_shipping.set('Brasil')
  CadastrarPage.new.type.select('Analyst').click
  CadastrarPage.new.industry.select('Energy').click
  CadastrarPage.new.annual_Revenue.set('R$35.000,00')
  CadastrarPage.new.employees.set('30' + rand(1000).to_s)
end

Então(/^um novo usuário será salvo$/) do
  CadastrarPage.new.button_salvar.click
end
