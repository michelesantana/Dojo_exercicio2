#language: pt
#utf-8

Funcionalidade: cadastrar um funcionário
  Eu como administrador do sistema
  Quero cadastrar um novo usuário
  Para manter atualizado os dados da minha empresa

  Contexto: acesso ao site
    Dado que eu esteja logado no site da empresa

  @cadastrar
  Esquema do Cenário: cadastrar um novo funcionário
   Quando eu acessar a opção create
   E inserir os dados para novo cadastro com o "<nome>"
   Então um novo usuário será salvo

   Exemplos:
     |nome|
     |Bruna|
     |Fatori|
     |Anderson|


  @editar
  Cenário: editar um usuário existente
    Quando eu acessar a opção contas será exibido as contas cadastradas
    Então conseguirei editar a conta

  @deletar
  Cenário: deletar um usuário existente
  Quando eu acessar a opção contas será exibido as contas cadastradas
  Então conseguirei deletar uma conta
